declare interface AppState {
    tab: string;
    navbarIsOpen: boolean;
    authModalIsOpen: boolean;
    loggedIn: function;
    token: string;
    width: number;
    height: number;
}

declare interface AuthState {
    uid: string;
    password: string;
    sent: boolean;
    errMsg: string;
    errCode: number;
    uidInputStyle: Object;
    passwordInputStyle: Object;
    requestProcessing: boolean;
}

declare interface ChartsState {
    pair: string;
    markets: string[];
    chartData: DataEntry[];
    difValues: DifValue[];
}

declare interface SignalsState {
    signalsInput: string;
}

declare interface ProfileState {
    uid: number | null;
    items: JSX.Element[];
    list: AnySignal[];
}