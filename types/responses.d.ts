declare interface CommonResponse {
    code: string;
    message: string;
}

declare interface AllSignalsResponse {
    code: string;
    message: AnySignal[];
}

declare interface SpecificSignalsResponse {
    code: string;
    message: SpecificSignal[];
}