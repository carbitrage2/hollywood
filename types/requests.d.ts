declare interface PasswordRequest {
    uid: number;
}

declare interface AuthRequest {
    uid: number;
    password: string;
}