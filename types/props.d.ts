declare interface NavbarProps {
    isOpen: boolean;
    loggedIn: boolean;
    toggleCollapse: function;
    toggleAuthModal: function;
    logOut: function;
    tab: string;
    setActiveTab: function;
}

declare interface ChartProps {
    pair: string;
    chartData: DataEntry[];
}

declare interface OptionsProps {
    pair: string;
    markets: string[];
    changePair: function;
    changeMarkets: function;
}

declare interface ChartsProps {
    loggedIn: boolean;
    logOut: function;
    deleteSignal: function;
    setActiveTab: function;
}

declare interface HomeProps {
    width: number;
    loggedIn: boolean;
    setActiveTab: function;
}

declare interface SignalsProps {
    markets: string[];
    difValues: DifValue[];
    getSignals: function;
    addSignal: function;
    deleteSignal: function;
    updateSignals: function;
}

declare interface ProfileProps {
    deleteSignal: function;
    setActiveTab: function;
    logOut: function;
}

declare interface AboutProps {
    setActiveTab: function;
}

declare interface LostProps {
    setActiveTab: function;
}