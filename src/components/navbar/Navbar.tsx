import React, { Component } from "react";
import { MDBNavbar, MDBNavbarBrand, MDBNavbarNav, MDBNavItem, MDBNavLink, MDBNavbarToggler, MDBCollapse, MDBDropdown,
MDBDropdownToggle, MDBDropdownMenu, MDBDropdownItem, MDBIcon } from "mdbreact";
import { Link } from "react-router-dom";

class Navbar extends Component<NavbarProps> {
  dropdownItem = {
    color: 'black',
    padding: '.25rem 1.5rem',
    fontWeight: 400,
    display: 'block',
    fontSize: 'inherit'
  }

  render() {
    let item: JSX.Element;
    if (this.props.loggedIn && !this.props.isOpen) {
      item =
        <MDBNavItem data-testid="userDropdown" data-status="signed">
          <MDBDropdown>
            <MDBDropdownToggle nav caret>
              <MDBIcon icon="user" />
            </MDBDropdownToggle>
            <MDBDropdownMenu right basic>
              <MDBDropdownItem style={ { padding: 0 } }>
                <Link style={ this.dropdownItem } to="/profile">
                  Profile
                </Link>
              </MDBDropdownItem>
              <MDBDropdownItem onClick={ this.props.logOut }>
                Log Out
              </MDBDropdownItem>
            </MDBDropdownMenu>
          </MDBDropdown>
        </MDBNavItem>;
    } else if (this.props.loggedIn && this.props.isOpen) {
      item =
        <MDBNavItem data-testid="userDropdown" data-status="signed">
          <MDBDropdown>
            <MDBDropdownToggle nav caret>
              <MDBIcon icon="user" />
            </MDBDropdownToggle>
            <MDBDropdownMenu left="true" basic>
              <MDBDropdownItem style={ { padding: 0 } } >
                <Link style={ this.dropdownItem } to="/profile" >
                  Profile
                </Link>
              </MDBDropdownItem>
              <MDBDropdownItem onClick={ this.props.logOut }>Log Out</MDBDropdownItem>
            </MDBDropdownMenu>
          </MDBDropdown>
        </MDBNavItem>;
    } else {
      item = 
        <MDBNavItem data-testid="userDropdown" data-status="unsigned">
          <MDBNavLink className="waves-effect waves-light" to="#!" onClick={ this.props.toggleAuthModal }>
            <MDBIcon icon="user" />
          </MDBNavLink>
        </MDBNavItem>;
    }
    return (
      <MDBNavbar color="primary-color" dark expand="md">
        <MDBNavbarBrand>
          <strong className="white-text">Carbi</strong>
        </MDBNavbarBrand>
        <MDBNavbarToggler onClick={ this.props.toggleCollapse } />
        <MDBCollapse id="navbarCollapse3" isOpen={ this.props.isOpen } navbar>
          <MDBNavbarNav left>
            <MDBNavItem active={ (this.props.tab === 'Home') ? true : false }>
              <MDBNavLink to="/" onClick={ () => { this.props.setActiveTab('Home') } }>Home</MDBNavLink>
            </MDBNavItem>
            <MDBNavItem active={ (this.props.tab === 'Charts') ? true : false }>
              <MDBNavLink to="/charts" onClick={ () => { this.props.setActiveTab('Charts') } }>Charts</MDBNavLink>
            </MDBNavItem>
            <MDBNavItem active={ (this.props.tab === 'About') ? true : false }>
              <MDBNavLink to="/about" onClick={ () => { this.props.setActiveTab('About') } }>About</MDBNavLink>
            </MDBNavItem>
          </MDBNavbarNav>
          <MDBNavbarNav right>
            { item }
          </MDBNavbarNav>
        </MDBCollapse>
      </MDBNavbar>
    );
  }
}

export default Navbar;