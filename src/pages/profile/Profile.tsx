import React, { Component } from 'react';
import { MDBContainer, MDBCard, MDBCardHeader, MDBCardBody, MDBCardTitle } from 'mdbreact';
import { Chip } from '@material-ui/core';
import { toast } from 'react-toastify';
import Cookies from 'universal-cookie';
import axios from 'axios';

class Profile extends Component<ProfileProps> {
  state: ProfileState = {
    uid: null,
    items: [] as JSX.Element[],
    list: [] as AnySignal[]
  } as ProfileState;

  createSignalsFromData(input: AnySignal[]) {
    this.setState({ list: input });
  }

  getUserID() {
    const cookies: Cookies = new Cookies();
    const token: string = cookies.get('token');
    if (token) {
      const headers: Object = { headers: { "Authorization": `Bearer ${token}` } }
      axios.get(`${process.env.REACT_APP_API}/user-data`, headers)
        .then(response => {
          const data: AllSignalsResponse = response.data as AllSignalsResponse;
          if (data.code === 'Success') {
            this.setState({
              uid: data.message
            });
          } else {
            console.error(data.message);
          }
        })
        .catch(error => {
          const data: CommonResponse = error.response.data as CommonResponse;
          toast.error(data.message, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true });
        })
    } else {
      this.props.logOut();
    }
  }

  getSignals() {
    const cookies: Cookies = new Cookies();
    const token: string = cookies.get('token');
    if (token) {
      const headers: Object = { headers: { "Authorization": `Bearer ${token}` } }
      axios.get(`${process.env.REACT_APP_API}/signals`, headers)
        .then(response => {
          const data: AllSignalsResponse = response.data as AllSignalsResponse;
          if (data.code === 'Success') {
            this.createSignalsFromData(data.message as AnySignal[]);
          } else {
            console.error(data.message);
          }
        })
        .catch(error => {
          const data: CommonResponse = error.response.data as CommonResponse;
          toast.error(data.message, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true });
        })
    } else {
      this.props.logOut();
    }
  }

  removeSignalFromList(item: AnySignal) {
    const itemToFind: string = JSON.stringify(item);
    for (const value of this.state.list) {
      const parsedValue: string = JSON.stringify(value);
      if (itemToFind === parsedValue) {
        let valueToRemove: number = this.state.list.indexOf(value);
        this.state.list.splice(valueToRemove, 1);
        this.setState({
          list: this.state.list
        });
        this.props.deleteSignal(value.id);
        break;
      }
    }
  }

  componentDidMount() {
    this.props.setActiveTab('');
    this.getUserID();
    this.getSignals();
  }

  render() {
    let items: JSX.Element[] = this.state.list.map((item: AnySignal) => {
      const label: string = `${ item.pair } (${ item.markets.join(', ') }): ${ item.difference.toString() }`;
      return <Chip 
        key={ this.state.list.indexOf(item) }
        label={ label } 
        color="default" 
        onDelete={ () => { this.removeSignalFromList(item) } } 
        style={ { marginBottom: '1em', marginRight: '1em' } }
      />;
    });
    return(
      <MDBContainer>
        <MDBCard border="light" className="m-3">
          <MDBCardHeader>Profile</MDBCardHeader>
          <MDBCardBody>
            <MDBCardTitle>User ID: { this.state.uid ? this.state.uid : 'NOT FOUND' }</MDBCardTitle>
            <MDBCardTitle>Signals: </MDBCardTitle>
            { items }
          </MDBCardBody>
        </MDBCard>
      </MDBContainer>
    );   
  }
}

export default Profile;